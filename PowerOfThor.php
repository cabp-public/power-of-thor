<?php
/**
 * Hint: You can use the debug stream to print initialTX and initialTY,
 * if Thor seems not follow your orders.
 **/

fscanf(STDIN, "%d %d %d %d",
    $lightX, // the X position of the light of power
    $lightY, // the Y position of the light of power
    $initialTX, // Thor's starting X position
    $initialTY // Thor's starting Y position
);

$tileMap = ['x' => 39, 'y' => 17];
$targetTile = ['x' => $lightX, 'y' => $lightY];
$currentTile = ['x' => $initialTX, 'y' => $initialTY];
$directionCoordinates = [
    'N' => [0, 0, 0, 1, 1, 0],
    'NE' => [1, 0, 0, 1, 0, 0],
    'E' => [1, 0, 0, 0, 0, 1],
    'SE' => [1, 1, 0, 0, 0, 0],
    'S' => [0, 1, 0, 0, 1, 0],
    'SW' => [0, 1, 1, 0, 0, 0],
    'W' => [0, 0, 1, 0, 0, 1],
    'NW' => [0, 0, 1, 1, 0, 0],
];

function getDirection(array $currentTile, array $targetTile, array $directionCoordinates)
{
    $directionToMove = 'NADA';

    $coordinatesToCheck = [
        intval($currentTile['x'] < $targetTile['x']),
        intval($currentTile['y'] < $targetTile['y']),
        intval($currentTile['x'] > $targetTile['x']),
        intval($currentTile['y'] > $targetTile['y']),
        intval($currentTile['x'] === $targetTile['x']),
        intval($currentTile['y'] === $targetTile['y']),
    ];

    foreach ($directionCoordinates as $directionKey => $coordinatesValue) {
        if ($coordinatesToCheck === $coordinatesValue) {
            $directionToMove = $directionKey;
            break;
        }
    }

    return $directionToMove;
}

function getNextTile(array $tileMap, array $currentTile, string $direction): array
{
    $nextX = $currentTile['x'];
    $nextY = $currentTile['y'];

    switch (strtoupper($direction)) {
        case 'N':
            $nextY = $currentTile['y'] - 1;
            break;

        case 'NE':
            $nextX = $currentTile['x'] + 1;
            $nextY = $currentTile['y'] - 1;
            break;

        case 'E':
            $nextX = $currentTile['x'] + 1;
            break;

        case 'SE':
            $nextX = $currentTile['x'] + 1;
            $nextY = $currentTile['y'] + 1;
            break;

        case 'S':
            $nextY = $currentTile['y'] + 1;
            break;

        case 'SW':
            $nextX = $currentTile['x'] - 1;
            $nextY = $currentTile['y'] + 1;
            break;

        case 'W':
            $nextX = $currentTile['x'] - 1;
            break;

        case 'NW':
            $nextX = $currentTile['x'] - 1;
            $nextY = $currentTile['y'] - 1;
            break;
    }

    // Boundaries
    $nextX = $nextX < 0 ? 0 : $nextX;
    $nextX = $nextX > $tileMap['x'] ? $tileMap['x'] : $nextX;
    $nextY = $nextY < 0 ? 0 : $nextY;
    $nextY = $nextY > $tileMap['y'] ? $tileMap['y'] : $nextY;

    return ['x' => $nextX, 'y' => $nextY];
}

while (TRUE)
{
    fscanf(STDIN, "%d", $remainingTurns);

    $directionToMove = getDirection($currentTile, $targetTile, $directionCoordinates);
    $currentTile = getNextTile($tileMap, $currentTile, $directionToMove);

    // To debug (equivalent to var_dump): error_log(var_export($var, true));

    echo("$directionToMove\n");
}
?>
